# cantonese_NMT

explore different methods and limits in the topic of Cantonese translation

##TODO
- [X] BART like pretraining
- [] Distillation from language model
- [] Modify to translate English
- [] Conditional generation
- [] Distill to transformer
- [] Speech translation???

## IMPORTANT NOTES
- do NOT use dropout in input (copy from other code base, one of the biggest mistake)
