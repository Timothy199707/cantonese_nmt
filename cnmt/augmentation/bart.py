import random
from copy import deepcopy

import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F

from loguru import logger

from cnmt.dataloader.supervise_dataset import Collate
from cnmt.dataloader.unsupervised_dataset import SemiSupervisedCollate


class BartAugmentation(SemiSupervisedCollate):
    def __init__(
        self,
        insertion_weight=0.2,
        reduction_weight=0.2,
        swaping_weight=0.2,
        repeating_weight=0.2,
        repeating_augment=2,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.insertion_weight = insertion_weight
        self.reduction_weight = reduction_weight
        self.swaping_weight = swaping_weight
        self.repeating_weight = repeating_weight
        self.repeating_augment = repeating_augment

    def _get_random_fraction(self):
        return random.uniform(0, 1)

    def token_swapping(self, text):
        tokens = self.tokenizer(text, return_tensors=False)
        idx = list(range(len(tokens)))
        total_samples = max(int(len(tokens) / 10), 1)
        for _ in range(total_samples):
            i1, i2 = random.sample(idx, 2)
            tokens[i1], tokens[i2] = tokens[i2], tokens[i1]
        return self.tokenizer.decode([tokens])[0]

    def token_inserting(self, text):
        tokens = self.tokenizer(text, return_tensors=False)
        total_samples = max(int(len(tokens) / 10), 1)
        random_vocabs = random.sample(
            list(range(len(self.tokenizer.vocab))), total_samples
        )
        idx = list(range(len(tokens)))
        indexs = random.sample(idx, total_samples)
        for i, random_vocab in zip(indexs, random_vocabs):
            try:
                tokens = tokens[:i] + [random_vocab] + tokens[i:]
            except Exception as e:
                print(e)
                print(tokens)
                print(i)
        return self.tokenizer.decode([tokens])[0]

    def token_reducing(self, text):
        tokens = self.tokenizer(text, return_tensors=False)
        total_samples = max(int(len(tokens) / 10), 1)
        idx = list(range(len(tokens)))
        indexs = random.sample(idx, total_samples)
        tokens = [t for i, t in enumerate(tokens) if i not in indexs]
        return self.tokenizer.decode([tokens])[0]

    def token_repeating(self, text):
        tokens = self.tokenizer(text, return_tensors=False)
        total_samples = max(int(len(tokens) / 10), 1)
        idx = list(range(len(tokens)))
        indexs = random.sample(idx, total_samples)
        for i in indexs:
            try:
                tokens = tokens[:i] + [tokens[i]] + tokens[i:]
            except Exception as e:
                print(e)
                print(tokens)
                print(i)
        return self.tokenizer.decode([tokens])[0]

    def augment(self, text):
        num = self._get_random_fraction()
        if num < self.insertion_weight:
            text = self.token_inserting(text)
        num = self._get_random_fraction()
        if num < self.reduction_weight:
            text = self.token_reducing(text)
        num = self._get_random_fraction()
        if num < self.swaping_weight:
            text = self.token_swapping(text)
        num = self._get_random_fraction()
        if num < self.repeating_weight:
            text = self.token_repeating(text)
        return text

    def __call__(self, batch):
        src_batch, trg_batch = zip(*[(b["unsup_src"], b["unsup_trg"]) for b in batch])
        max_length = self.max_length
        max_length_trg = self.max_length

        src_batch_aug = deepcopy(src_batch)
        trg_batch_aug = deepcopy(trg_batch)

        src_batch = [self.augment(src) for src in src_batch]
        trg_batch = [self.augment(trg) for trg in trg_batch]

        # TODO: this is a bug, the sentence havent't been tokenized!!!!
        if self.sort_by_length:
            src_batch, trg_batch, max_length, max_length_trg = self.get_max_length(
                src_batch, trg_batch
            )
            max_length = min(max_length, self.max_length)
            max_length_trg = min(max_length_trg, self.max_length)

        src_batch_aug = self.tokenizer(list(src_batch_aug), max_length=max_length)
        trg_batch_aug = self.tokenizer(list(trg_batch_aug), max_length=max_length_trg)

        src = self.tokenizer(list(src_batch), max_length=max_length)
        src_len = (src != self.tokenizer.pad_token_id).sum(-1)

        trg = self.tokenizer(list(trg_batch), max_length=max_length_trg)
        trg_len = (trg != self.tokenizer.pad_token_id).sum(-1)

        if self.sort_by_length:
            src, src_len = self.length_sort_with_len(src, src_len)
            trg, trg_len = self.length_sort_with_len(trg, trg_len)

        result = (src, src_len, src_batch_aug, trg, trg_len, trg_batch_aug)

        return result
