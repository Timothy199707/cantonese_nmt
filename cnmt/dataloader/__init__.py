import copy
import random

import importlib
from typing import Optional
import pytorch_lightning as pl
import torch
from torch.utils.data import random_split, DataLoader
from loguru import logger
from cnmt.util import sort2list
import torch.utils.data
from random import randint
import logging


class DefaultCollate:
    """
    Create collate function cup-holder
    """

    def __init__(self, processors=[], result_names=[], return_dict=True, **kwargs):
        self.processors = processors
        self.result_names = result_names
        self.return_dict = return_dict
        self.__dict__.update(kwargs)

    def __call__(self, batch):
        # pass the batch (default to be dict) to all the processor
        if self.return_dict:
            new_batch = {}
            for processor, name in zip(self.processors, self.result_names):
                new_batch[name] = processor(batch)
            return new_batch
        else:
            result = tuple()
            for processor in self.processors:
                result += processor(batch)
            return result


class ConcatDataset(torch.utils.data.Dataset):
    def __init__(self, datasets=[], return_dict=True):
        self.datasets = datasets
        self.return_dict = return_dict

    def get_cycle_i(self, i, dataset_size):

        data_frac = int(dataset_size / self.__len__() * random.uniform(0, 1))

        cycled_i = i + self.__len__() * data_frac
        while cycled_i > dataset_size:
            cycled_i = cycled_i - dataset_size
        return cycled_i

    def __getitem__(self, i):
        result = []
        for dataset in self.datasets:
            if len(dataset) > self.__len__():
                cycled_i = self.get_cycle_i(i, len(dataset))

            else:
                cycled_i = i
            result.append(dataset[cycled_i])

        if not self.return_dict:
            return tuple(result)
        else:
            dict_result = {}
            for r in result:
                dict_result.update(r)
            return dict_result

    def __len__(self):
        return min(len(d) for d in self.datasets)


class AutoDataModule(pl.LightningDataModule):
    def __init__(
        self,
        datasets=None,
        collate_fn=None,
        split_frac=0.1,
        batch_size=32,
        num_workers=16,
        collate_kwargs=None,
        safe=True,
        **kwargs,
    ):

        super().__init__(**kwargs)
        if isinstance(datasets, list):
            if not collate_kwargs:
                self.dataset = ConcatDataset(*datasets)
            else:
                self.dataset = ConcatDataset(*datasets, **collate_kwargs)
        else:
            self.dataset = datasets

        if safe:
            self.dataset = SafeDataset(self.dataset)

        self.split_frac = split_frac
        self.collate_fn = collate_fn
        self.batch_size = batch_size
        self.num_workers = num_workers

    def prepare_data(self):
        # download
        pass

    def setup(self, stage: Optional[str] = None):
        # Assign train/val datasets for use in dataloaders
        if stage == "fit" or stage is None:

            train_size = int(self.dataset.__len__() * (1 - self.split_frac))
            val_size = self.dataset.__len__() - train_size
            split_sizes = [train_size, val_size]
            self.train_dataset, self.val_dataset = random_split(
                self.dataset, split_sizes
            )

        # Assign test dataset for use in dataloader(s)
        if stage == "test" or stage is None:
            self.test_dataset = self.dataset

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            shuffle=True,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )


class SafeDataset(torch.utils.data.Dataset):
    """A wrapper around a torch.utils.data.Dataset that allows dropping
    samples dynamically.
    """

    def __init__(self, dataset, eager_eval=False):
        """Creates a `SafeDataset` wrapper around `dataset`."""
        self.dataset = dataset
        self.eager_eval = eager_eval
        # These will contain indices over the original dataset. The indices of
        # the safe samples will go into _safe_indices and similarly for unsafe
        # samples.
        self._safe_indices = []
        self._unsafe_indices = []

        # If eager_eval is True, we can simply go ahead and build the index
        # by attempting to access every sample in self.dataset.
        if self.eager_eval is True:
            self._build_index()

    def _build_index(self):
        for idx in range(len(self.dataset)):
            # The returned sample is deliberately discarded because
            # self._unsafe_get_item(idx) is called only to classify every index
            # into either safe_samples_indices or _unsafe_samples_indices.
            _ = self._unsafe_get_item(idx)

    def _reset_index(self):
        """Resets the safe and unsafe samples indices."""
        self._safe_indices = self._unsafe_indices = []

    def __getattr__(self, key):
        """Delegates to original dataset object if an attribute is not
        found in this class.
        """
        #! This does not work with NCCL backend, __getstate__ __setstate__ will result in recursion error
        return getattr(self.dataset, key)

    def _indexed_get_item(self, idx):
        return self.dataset[self._safe_indices[idx]]

    def _get_item(self, idx):
        """Returns None instead of throwing an error when dealing with an
        unsafe sample, and also builds an index of safe and unsafe samples as
        and when they get accessed.
        """
        try:
            # differentiates IndexError occuring here from one occuring during
            # sample loading
            invalid_idx = False
            if idx >= len(self.dataset):
                invalid_idx = True
                raise IndexError
            sample = self.dataset[idx]
            if idx not in self._safe_indices:
                self._safe_indices.append(idx)
            return sample
        except Exception as e:
            logging.warning(e)
            if isinstance(e, IndexError):
                if invalid_idx:
                    raise
            if idx not in self._unsafe_indices:
                self._unsafe_indices.append(idx)
            return None

    @property
    def is_index_built(self):
        """Returns True if all indices of the original dataset have been
        classified into safe_samples_indices or _unsafe_samples_indices.
        """
        return len(self.dataset) == len(self._safe_indices) + len(self._unsafe_indices)

    @property
    def num_samples_examined(self):
        return len(self._safe_indices) + len(self._unsafe_indices)

    def __len__(self):
        """Returns the length of the original dataset.
        NOTE: This is different from the number of actually valid samples.
        """
        return len(self._safe_indices) if self.is_index_built else len(self.dataset)

    def __iter__(self):
        return (self.__getitem__(i) for i in range(len(self)))

    def __getitem__(self, idx):
        """Behaves like the standard __getitem__ for Dataset when the index
        has been built.
        """
        get_fn = self._indexed_get_item if self.is_index_built else self._get_item
        ret = None
        while ret is None:
            try:
                ret = get_fn(idx)
            except Exception as e:
                logging.warning(e)
            idx = randint(0, len(self) - 1)
        return ret


def get_collate_construct_fn(desc, fn_name, **kwargs):
    def construct_collate(tokenizer=None, **kwargs):
        current_desc = copy.deepcopy(desc)

        current_desc.update(kwargs)

        fname, function_name = fn_name.split(".")
        module = importlib.import_module(f"cnmt.dataloader.{fname}")
        fn = getattr(module, function_name, None)
        current_desc.update(kwargs)
        collate_fn = fn(**current_desc)
        return {"collate_fn": collate_fn, **kwargs}

    return [construct_collate]


def get_dataset_construct_fn(desc, fn_name, **kwargs):
    def construct_dataset(**kwargs):
        current_desc = copy.deepcopy(desc)

        current_desc.update(kwargs)

        fname, function_name = fn_name.split(".")
        module = importlib.import_module(f"cnmt.dataloader.{fname}")
        fn = getattr(module, function_name, None)
        current_desc.update(kwargs)
        dataset = fn(**current_desc)
        return {"dataset": dataset, **kwargs}

    return [construct_dataset]


def get_construct_fn(desc={}, fn_name=None, module_name=None, **kwargs):
    def construct_(**kwargs):
        current_desc = copy.deepcopy(desc) if desc else {}

        current_desc.update(kwargs)

        fname, function_name = fn_name.split(".")
        module = importlib.import_module(f"cnmt.dataloader.{fname}")
        fn = getattr(module, function_name, None)
        dataloader = fn(**current_desc)
        return {module_name: dataloader, **kwargs}

    return [construct_]


def get_dataloader_construct_fn(desc=None, data_module_name=None, **kwargs):
    def get_dataloader(**kwargs):
        assert data_module_name in kwargs

        dataloader = kwargs[data_module_name]

        return {"dataloader": dataloader, **kwargs}

    return [get_dataloader]
