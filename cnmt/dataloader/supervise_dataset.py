import pytorch_lightning as pl
import torch
from torch.utils.data import random_split, DataLoader
import regex as re
from typing import Optional

from cnmt.util import sort2list


class Collate:
    def __init__(
        self,
        tokenizer=None,
        max_length=None,
        sort_by_length=True,
        trg2src_indexing=False,
        **kwargs
    ):
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.sort_by_length = sort_by_length
        self.trg2src_indexing = trg2src_indexing

    def get_max_length(self, src_batch, trg_batch):
        # batch index
        index = list(range(len(src_batch)))

        length = [len(tokens) for tokens in src_batch]
        length_trg = [len(tokens) for tokens in trg_batch]
        max_length, max_length_trg = max(length), max(length_trg)

        return src_batch, trg_batch, max_length, max_length_trg

    def get_descending_index(self, src_len):
        descending_index = torch.argsort(src_len, dim=0, descending=True)
        return descending_index

    def length_sort(self, src_batch, trg_batch, src_len):

        descending_index = torch.argsort(src_len, dim=0, descending=True)

        src_len = src_len[descending_index]
        src_batch = src_batch[descending_index]
        trg_batch = trg_batch[descending_index]

        return src_batch, trg_batch, src_len

    def __call__(self, batch):
        src_batch, trg_batch = zip(*[(b["src_batch"], b["trg_batch"]) for b in batch])
        max_length = self.max_length
        max_length_trg = self.max_length

        if self.sort_by_length:
            src_batch, trg_batch, max_length, max_length_trg = self.get_max_length(
                src_batch, trg_batch
            )

        src = self.tokenizer(list(src_batch), max_length=max_length)
        src_len = (src != self.tokenizer.pad_token_id).sum(-1)

        trg = self.tokenizer(list(trg_batch), max_length=max_length_trg)

        if self.sort_by_length:
            src, trg, src_len = self.length_sort(src, trg, src_len)

        result = (
            src,
            src_len,
            trg,
        )

        if self.trg2src_indexing:
            trg_len = (trg != self.tokenizer.pad_token_id).sum(-1)
            trg_index = self.get_descending_index(trg_len)
            result += (
                trg_len,
                trg_index,
            )
        else:
            result += (
                None,
                None,
            )

        return result


class SupervisedTranslationDataSet(torch.utils.data.Dataset):
    def __init__(
        self, src_fname: str = "src.txt", trg_fname: str = "trg.txt", **kwargs
    ):

        super(SupervisedTranslationDataSet).__init__()

        self.src_text = self.read_txt_files(src_fname)
        self.trg_text = self.read_txt_files(trg_fname)

    def read_txt_files(self, fname):
        with open(fname, "r") as f:
            lines = [re.sub(r"\n", "", line) for line in f.readlines() if line]
        return lines

    def __len__(self):
        return len(self.src_text)

    def __getitem__(self, index):
        src = self.src_text[index]
        trg = self.trg_text[index]

        return {"src_batch": src, "trg_batch": trg}


class MultiFileTranslationDataSet(torch.utils.data.Dataset):
    def __init__(self, src_fname_list: list = [], trg_fname_list: list = [], **kwargs):
        super().__init__()
        self.src_text = sum(
            [self.read_txt_files(src_fname) for src_fname in src_fname_list], []
        )
        self.trg_text = sum(
            [self.read_txt_files(trg_fname) for trg_fname in trg_fname_list], []
        )

    def read_txt_files(self, fname):
        with open(fname, "r") as f:
            lines = [re.sub(r"\n", "", line) for line in f.readlines() if line]
        return lines

    def __len__(self):
        return len(self.src_text)

    def __getitem__(self, index):
        src = self.src_text[index]
        trg = self.trg_text[index]

        return {"src_batch": src, "trg_batch": trg}


class MultiFileTranslationDataModule(pl.LightningDataModule):
    def __init__(
        self,
        src_fname_list: list = [],
        trg_fname_list: list = [],
        collate_fn=None,
        split_frac=0.1,
        batch_size=32,
        num_workers=16,
        **kwargs
    ):
        super().__init__()
        self.src_fname_list = src_fname_list
        self.trg_fname_list = trg_fname_list
        self.collate_fn = collate_fn
        self.split_frac = split_frac
        self.batch_size = batch_size
        self.num_workers = num_workers

    def prepare_data(self):
        # download
        pass

    def setup(self, stage: Optional[str] = None):

        # Assign train/val datasets for use in dataloaders
        if stage == "fit" or stage is None:
            data_full = MultiFileTranslationDataSet(
                self.src_fname_list, self.trg_fname_list
            )
            train_size = int(data_full.__len__() * self.split_frac)
            val_size = data_full.__len__() - train_size
            split_sizes = [train_size, val_size]
            self.train_dataset, self.val_dataset = random_split(data_full, split_sizes)

        # Assign test dataset for use in dataloader(s)
        if stage == "test" or stage is None:
            self.test_dataset = MultiFileTranslationDataSet(
                self.src_fname_list, self.trg_fname_list
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )


class SupervisedTranslationDataModule(pl.LightningDataModule):
    def __init__(
        self,
        data_dir: str = "./",
        src_fname: str = "src.txt",
        trg_fname: str = "trg.txt",
        collate_fn=None,
        split_frac=0.1,
        batch_size=32,
        **kwargs
    ):
        super().__init__()
        self.src_fname = data_dir + src_fname
        self.trg_fname = data_dir + trg_fname
        self.collate_fn = collate_fn
        self.split_frac = split_frac
        self.batch_size = batch_size

    def prepare_data(self):
        # download
        pass

    def setup(self, stage: Optional[str] = None):

        # Assign train/val datasets for use in dataloaders
        if stage == "fit" or stage is None:
            data_full = SupervisedTranslationDataSet(self.src_fname, self.trg_fname)
            train_size = int(data_full.__len__() * self.split_frac)
            val_size = data_full.__len__() - train_size
            split_sizes = [train_size, val_size]
            self.train_dataset, self.val_dataset = random_split(data_full, split_sizes)

        # Assign test dataset for use in dataloader(s)
        if stage == "test" or stage is None:
            self.test_dataset = SupervisedTranslationDataSet(
                self.src_fname, self.trg_fname
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset, batch_size=self.batch_size, collate_fn=self.collate_fn
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test, batch_size=self.batch_size, collate_fn=self.collate_fn
        )


if __name__ == "__main__":
    from cnmt.tokenizer.space_sep_tokenizer import SpaceSepTokenizer

    tokenizer = SpaceSepTokenizer(
        vocab_fname="/home/vincent/data/yue2zh_all/text.vocab"
    )
    collate_fn = Collate(tokenizer)
    dataset = SupervisedTranslationDataSet(
        src_fname="/home/vincent/data/yue2zh_all/new_su_train_1.txt",
        trg_fname="/home/vincent/data/yue2zh_all/new_su_train_0.txt",
    )
    loader = iter(DataLoader(dataset, batch_size=2, collate_fn=collate_fn))
    print(next(loader))

    # data_module = SupervisedTranslationDataModule(
    #        data_dir = "/home/vincent/data/yue2zh_all/",
    #        src_fname = "new_su_train_1.txt",
    #        trg_fname = "new_su_train_0.txt",
    #        collate_fn=collate_fn)
