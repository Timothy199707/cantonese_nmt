import pytorch_lightning as pl
import torch
from torch.utils.data import random_split, DataLoader
import regex as re
from typing import Optional
from itertools import cycle
import random

from cnmt.util import sort2list
from cnmt.dataloader.supervise_dataset import (
    MultiFileTranslationDataSet,
    Collate,
    MultiFileTranslationDataModule,
)


class SemiSupervisedCollate(Collate):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def length_sort_with_len(self, src_batch, src_len):

        descending_index = torch.argsort(src_len, dim=0, descending=True)

        src_len = src_len[descending_index]
        src_batch = src_batch[descending_index]

        return src_batch, src_len

    def __call__(self, batch):

        src_batch, trg_batch = zip(*[(b["unsup_src"], b["unsup_trg"]) for b in batch])
        max_length = self.max_length
        max_length_trg = self.max_length

        # TODO: this is a bug, the sentence havent't been tokenized!!!!
        if self.sort_by_length:
            src_batch, trg_batch, max_length, max_length_trg = self.get_max_length(
                src_batch, trg_batch
            )
            max_length = min(max_length, self.max_length)

        src = self.tokenizer(list(src_batch), max_length=max_length)
        src_len = (src != self.tokenizer.pad_token_id).sum(-1)

        trg = self.tokenizer(list(trg_batch), max_length=max_length_trg)
        trg_len = (trg != self.tokenizer.pad_token_id).sum(-1)

        if self.sort_by_length:
            src, src_len = self.length_sort_with_len(src, src_len)
            trg, trg_len = self.length_sort_with_len(trg, trg_len)

        result = (src, src_len, trg, trg_len)

        return result


class UnSupervisedCollate(SemiSupervisedCollate):
    def __init__(self, data_name=None, return_dict=False, **kwargs):
        super().__init__(**kwargs)
        self.data_name = data_name
        self.return_dict = return_dict

    def __call__(self, batch, **kwargs):
        if isinstance(batch, list):
            batch = [b[self.data_name] for b in batch]
        else:
            batch = batch[self.data_name]

        max_length = self.max_length
        max_length_trg = self.max_length
        src_batch, _, max_length, _ = self.get_max_length(batch, batch)
        max_length = min(max_length, self.max_length)
        batch = self.tokenizer(list(batch), max_length=max_length)
        _len = (batch != self.tokenizer.pad_token_id).sum(-1)
        if self.sort_by_length:
            batch, _len = self.length_sort_with_len(batch, _len)
        if not self.return_dict:
            result = (batch, _len)
        else:
            result = {self.data_name: {"tokens": batch, "len": _len}, **kwargs}
        return result


class MultiFileTextDataSet(torch.utils.data.Dataset):
    def __init__(self, fn_list: list = [], data_name="unsup_src", **kwargs):
        super().__init__(**kwargs)
        self.unsup_text = sum(
            [self.read_txt_files(src_fname) for src_fname in fn_list], []
        )
        self.data_name = data_name

    def read_txt_files(self, fname):
        with open(fname, "r") as f:
            lines = [re.sub(r"\n", "", line) for line in f.readlines() if line]
        return lines

    def __len__(self):
        return len(self.unsup_text)

    def __getitem__(self, index):
        return {self.data_name: self.unsup_text[index]}


class SemiMultiFileTranslationDataModule(MultiFileTranslationDataModule):
    def __init__(
        self,
        src_unsupervised_fname_list: list = [],
        trg_unsupervised_fname_list: list = [],
        **kwargs
    ):
        super().__init__(**kwargs)
        self.src_unsupervised_fname_list = src_unsupervised_fname_list
        self.trg_unsupervised_fname_list = trg_unsupervised_fname_list

    def prepare_data(self):
        # download
        pass

    def setup(self, stage: Optional[str] = None):

        # Assign train/val datasets for use in dataloaders
        if stage == "fit" or stage is None:
            data_full = MultiFileSemiSupervisedTranslationDataSet(
                src_fname_list=self.src_fname_list,
                trg_fname_list=self.trg_fname_list,
                unsup_src_fn_list=self.src_unsupervised_fname_list,
                unsup_trg_fn_list=self.trg_unsupervised_fname_list,
            )
            train_size = int(data_full.__len__() * self.split_frac)
            val_size = data_full.__len__() - train_size
            split_sizes = [train_size, val_size]
            self.train_dataset, self.val_dataset = random_split(data_full, split_sizes)

        # Assign test dataset for use in dataloader(s)
        if stage == "test" or stage is None:
            self.test_dataset = MultiFileTranslationDataSet(
                self.src_fname_list, self.trg_fname_list
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            collate_fn=self.collate_fn,
            num_workers=self.num_workers,
        )


if __name__ == "__main__":
    yaml_str = """
train_both_ways: true
batch_size: 1
src_fname_list:
  - '/home/vincent/data/full_data/full_yue.txt'
trg_fname_list:
  - '/home/vincent/data/full_data/full_zh.txt'

src_unsupervised_fname_list:
  - '/home/vincent/data/yue2zh_all/train_0.txt'
trg_unsupervised_fname_list:
  - '/home/vincent/data/yue2zh_all/train_0.txt'

# construct tokenizer
Tokenizer: !new:cnmt.tokenizer.space_sep_tokenizer.SpaceSepTokenizer
  sep: " " 
  remove_empty: false
  vocab_fname: "/home/vincent/data/yue2zh_all/text.vocab"

# tokenizer config
vocab: !apply:getattr
  - !ref <Tokenizer>
  - vocab
vocab_size: !apply:len
  - !ref <vocab>
pad_idx: !apply:getattr
  - !ref <Tokenizer>
  - pad_token_id

# data processing
Collate: !new:cnmt.dataloader.supervise_dataset.Collate
  tokenizer: !ref <Tokenizer> 
  trg2src_indexing: !ref <train_both_ways>

UnsupervisedCollate: !new:cnmt.dataloader.unsupervised_dataset.SemiSupervisedCollate
  tokenizer: !ref <Tokenizer> 
  trg2src_indexing: !ref <train_both_ways>

DataModule: !new:cnmt.dataloader.supervise_dataset.MultiFileTranslationDataModule
  src_fname_list: !ref <src_fname_list>
  trg_fname_list: !ref <trg_fname_list>
  num_workers: 16
  split_frac: 0.1
  batch_size: !ref <batch_size>
  collate_fn: !ref <Collate>

SemiSuperiseDataModule: !new:cnmt.dataloader.unsupervised_dataset.SemiMultiFileTranslationDataModule
  unsup_src_fn_list: !ref <src_unsupervised_fname_list>
  unsup_trg_fn_list: !ref <trg_unsupervised_fname_list>
  src_fname_list: !ref <src_fname_list>
  trg_fname_list: !ref <trg_fname_list>
  num_workers: 16
  split_frac: 0.1
  batch_size: !ref <batch_size>
  collate_fn: !ref <UnsupervisedCollate>

    """

    from hyperpyyaml import load_hyperpyyaml

    train_modules = load_hyperpyyaml(yaml_str)
    train_modules["SemiSuperiseDataModule"].setup("fit")
    dataloader = train_modules["SemiSuperiseDataModule"].train_dataloader()
    print(next(iter(dataloader)))
