from hyperpyyaml import load_hyperpyyaml
from pathlib import Path


def load_hyper_config(fname):
    with open(fname, "r") as f:
        yaml_str = f.read()
        train_modules = load_hyperpyyaml(yaml_str)
    return train_modules, yaml_str


def add_name_to_dir(name, directory):
    return directory + name + "/"


def save_config(config_string, training_config):

    checkpoint_dir = training_config["checkpoint_dir"]

    # if "version" in training_config:
    #    checkpoint_dir += f"version_{training_config['version']}/"
    Path(checkpoint_dir).mkdir(parents=True, exist_ok=True)

    with open(checkpoint_dir + "/training_config.yaml", "w") as f:
        f.write(config_string)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str)
    config_fname = getattr(parser.parse_args(), "config")
    config, yaml_str = load_hyper_config(config_fname)

    save_config(yaml_str, config)

    training_dict = config["train"]
    kwargs = (
        training_dict["training_kwargs"] if training_dict["training_kwargs"] else {}
    )
    training_dict["trainer"].fit(*training_dict["training_args"], **kwargs)
