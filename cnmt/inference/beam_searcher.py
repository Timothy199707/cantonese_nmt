import torch
from torch.nn.functional import normalize


class BeamTree:
    def __init__(self, topk=10, start_id=0, end_id=1, collate_fn=None, **kwargs):
        self.topk = topk
        self.start_id = start_id
        self.end_id = end_id

        self.collate_fn = collate_fn

    def init_beam_state(self):
        self.beam_set = [[start_id]]
        self.beam_prop = torch.tensor([0])
        self.search_result = []
        self.search_result_prob = []
        self.tmp_topk = self.topk

    def get_input(self):
        return self.collate_fn(self.beam_set)

    def update(self, log_prob, states):
        """
        prob: torch tensor (current topk size, vocab size)
        """
        beam_size, vocab_size = log_prob.shape

        assert len(self.beam_set) == beam_size
        updated_probs = log_prob + self.beam_prop.unsqueeze(-1).repeat(1, vocab_size)
        topk_id = torch.topk(updated_probs.flatten(), self.tmp_topk, -1)
        topk_prob = updated_probs[topk_id]

        beam_ids_ = [(id_ // beam_size, id_ % beam_size) for id_ in topk_id]
        new_beam_set = []
        new_beam_prob = []
        new_beam_state = []

        for (beam_id, vocab_id), p, state in zip(beam_ids, topk_prob, states):
            if self.tmp_topk >= len(new_beam_set):
                break
            if self.tmp_topk <= 0:
                break
            if vocab_id == end_id:
                self.search_result.append(self.beam_set[beam_id] + [self.end_id])
                self.search_result_prob.append(p)
                self.tmp_topk = self.tmp_topk - 1
            new_beam_set.append(self.beam_set[beam_id] + [int(vocab_id)])
            new_beam_prob.append(p)
            new_beam_state.append(state)

        self.beam_set = new_beam_set
        self.beam_prop = torch.stack(new_beam_prob)
        new_batch = self.get_input()
        new_beam_state = torch.stake(new_beam_state, dim=0)
        return new_batch, new_beam_state


class DefaultRNNBeamSearcher(BeamTree):
    """
    Autoregressive beam searcher
    (not support batch decoding yet)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def enc_dec_search(self, src, src_len, enc, dec, **kwargs):
        """
        enc: input (src, src_len) -> output (hidden, current state)
        dec: input (input, hidden, enc_state) -> output (output, hidden)
        """
        self.init_beam_state()
        hidden, enc_state = enc(src, src_len)
        inputs = self.get_input()
        output, hidden = dec(inputs, hidden, enc_state)
        while self.tmp_topk > 0:
            inputs, hidden = self.update(output, hidden)
            current_enc_repeat = (inputs.size(0),) + (1,) * len(hidden.shape[1:])
            current_enc_state = enc_state.repeat(*current_enc_repeat)
            output, hidden = dec(inputs, hidden, current_enc_state)

        return self.search_result, self.search_result_prob

    def search(self, model, init_hidden, enc=None):
        pass


class PseudoGenerator:
    """
    this is a debug generator for testing the BeamTree class
    """

    def __init__(self, num_vocab):
        self.num_vocab = num_vocab
        pass

    def get_prob_from_previous_state(self, **kwargs):
        return normalize(torch.random(self.num_vocab))
