import random
import torch
from torch.nn.functional import normalize


def create_batch_of_bos_token(tokenizer, batch_size):
    token = torch.ones(batch_size) * tokenizer.bos_token_id
    return token


def hidden_to_cuda(hidden):
    # if the model is lstm:
    if isinstance(hidden, tuple):
        hidden = tuple(h.cuda() for h in hidden)
    else:
        hidden = hidden.cuda()
    return hidden


def batch_greedy_decode_no_context(
    model, tokenizer, batch_size, max_seq_length=25, gpu=False
):
    inputs = create_batch_of_bos_token(tokenizer, batch_size).long()
    hidden = model.init_hidden(batch_size)
    if gpu:
        inputs = inputs.cuda()
        hidden = hidden_to_cuda(hidden)

    full_sentence = [inputs]
    for i in range(max_seq_length + 2):
        out, hidden = model(inputs, hidden)
        inputs = torch.argmax(out, dim=-1)
        full_sentence.append(inputs)
    full_sentence = torch.stack(full_sentence, dim=-1)
    full_sentence = tokenizer.decode(full_sentence)
    return full_sentence


def batch_random_decode_no_context(
    model, tokenizer, batch_size, max_seq_length=25, gpu=False
):
    inputs = create_batch_of_bos_token(tokenizer, batch_size).long()
    hidden = model.init_hidden(batch_size)
    if gpu:
        inputs = inputs.cuda()
        hidden = hidden_to_cuda(hidden)

    full_sentence = [inputs]
    for i in range(max_seq_length + 2):
        out, hidden = model(inputs, hidden)
        out_prob = torch.nn.functional.softmax(out)
        inputs = torch.multinomial(out_prob, 1).squeeze(-1)
        full_sentence.append(inputs)

    full_sentence = torch.stack(full_sentence, dim=-1)
    full_sentence = tokenizer.decode(full_sentence)

    return full_sentence


class PseudoTokenizer:
    """
    tokenizer for debug purpose
    """

    def __init__(self, bos_token_id=0):
        self.bos_token_id = bos_token_id


class PseudoGenerator:
    """
    this is a debug generator for testing the greedy decoding
    mimicing a simple RNNLM
    """

    def __init__(self, num_vocab, eos_token_id=1, end_token_prop=0.1, batch_size=10):
        self.num_vocab = num_vocab
        self.eos_token_id = eos_token_id
        self.end_token_prop = end_token_prop
        self.batch_size = batch_size
        end_logit = torch.zeros(self.num_vocab)
        end_logit[self.eos_token_id] = 1
        self.end_logit = end_logit
        pass

    def init_hidden(self, *args, **kwargs):
        return None

    def __call__(self, *args, **kwargs):
        end_prob = torch.where(torch.rand(self.batch_size) < self.end_token_prop)[0]
        generated_logit = torch.rand(self.batch_size, self.num_vocab)
        if end_prob.nelement() > 0:
            generated_logit[end_prob] = self.end_logit.clone()

        return generated_logit, None


if __name__ == "__main__":
    batch_size = 2
    trial_tokenizer = PseudoTokenizer()
    from cnmt.tokenizer.space_sep_tokenizer import SpaceSepTokenizer

    tokenizer = SpaceSepTokenizer(
        vocab_fname="/home/vincent/data/yue2zh_all/text.vocab"
    )
    trial_module = PseudoGenerator(
        len(tokenizer.vocab), eos_token_id=tokenizer.eos_token_id, batch_size=batch_size
    )

    result = batch_greedy_decode_no_context(trial_module, tokenizer, batch_size)
    print(result)
