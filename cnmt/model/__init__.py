import copy
import importlib
import random

from loguru import logger


def get_model_construct_fn(desc, model_name=None, **kwargs):
    def construct_model(**kwargs):
        current_desc = copy.deepcopy(desc)
        fname, function_name = model_name.split(".")
        module = importlib.import_module(f"cnmt.model.{fname}")
        fn = getattr(module, function_name, None)
        current_desc.update(kwargs)
        trainer_instance = fn(**current_desc)
        return {"model": trainer_instance, **kwargs}

    return [construct_model]
