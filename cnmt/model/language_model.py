import random

import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence
from loguru import logger

from torch.autograd import Variable


class LM_LSTM(nn.Module):
    """Simple LSMT-based language model"""

    def __init__(
        self,
        embedding_dim,
        vocab_size,
        num_layers,
        dp_keep_prob,
        rnn_module=nn.LSTM,
        dropout=0.5,
    ):
        super(LM_LSTM, self).__init__()
        self.embedding_dim = embedding_dim
        self.vocab_size = vocab_size
        self.dp_keep_prob = dp_keep_prob
        self.num_layers = num_layers
        self.dropout = nn.Dropout(1 - dp_keep_prob)
        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.rnn_module = rnn_module
        self.lstm = self.rnn_module(
            input_size=embedding_dim,
            hidden_size=embedding_dim,
            num_layers=num_layers,
            dropout=dropout,
        )
        self.sm_fc = nn.Linear(in_features=embedding_dim, out_features=vocab_size)
        self.init_weights()

    def init_weights(self):
        init_range = 0.1
        self.word_embeddings.weight.data.uniform_(-init_range, init_range)
        self.sm_fc.bias.data.fill_(0.0)
        self.sm_fc.weight.data.uniform_(-init_range, init_range)

    def init_hidden(self, batch_size):
        if self.rnn_module == nn.LSTM:
            return (
                torch.zeros((self.num_layers, batch_size, self.embedding_dim)),
                torch.zeros((self.num_layers, batch_size, self.embedding_dim)),
            )
        else:
            return torch.zeros((self.num_layers, batch_size, self.embedding_dim))

    def forward(self, inputs, hidden):
        embeds = self.dropout(self.word_embeddings(inputs))
        if len(list(embeds.shape)) < 3:
            embeds = embeds.unsqueeze(0)
        lstm_out, hidden = self.lstm(embeds, hidden)
        lstm_out = self.dropout(lstm_out)
        logits = self.sm_fc(lstm_out.view(-1, self.embedding_dim))
        return logits, hidden


class ResidualLSTM(LM_LSTM):
    def __init__(
        self,
        embedding_dim,
        vocab_size,
        num_layers,
        dp_keep_prob,
        rnn_module=nn.LSTM,
        dropout=0.5,
    ):
        super(LM_LSTM, self).__init__()
        self.embedding_dim = embedding_dim
        self.vocab_size = vocab_size
        self.dp_keep_prob = dp_keep_prob
        self.num_layers = num_layers
        self.dropout = nn.Dropout(1 - dp_keep_prob)
        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.rnn_module = rnn_module

        self.lstm = nn.ModuleList(
            [
                self.rnn_module(
                    input_size=embedding_dim,
                    hidden_size=embedding_dim,
                    num_layers=1,
                    dropout=0,
                )
                for i in range(num_layers)
            ]
        )

        self.dropout_layers = nn.ModuleList(
            [nn.Dropout(dropout) for i in range(num_layers - 1)]
        )

        self.sm_fc = nn.Linear(in_features=embedding_dim, out_features=vocab_size)
        self.init_weights()

    def forward(self, inputs, hidden):
        embeds = self.dropout(self.word_embeddings(inputs))
        if len(list(embeds.shape)) < 3:
            embeds = embeds.unsqueeze(0)
        for i, m in enumerate(self.lstm):
            if self.rnn_module == nn.LSTM:
                h = (hidden[0][i].unsqueeze(0), hidden[1][i].unsqueeze(0))
            else:
                h = hidden[i].unsqueeze(0)

            tmp_embeds, _ = m(embeds, h)
            if i > 0:
                d = self.dropout_layers[i - 1]
                embeds = d(embeds)
            embeds = embeds + tmp_embeds
        lstm_out = self.dropout(embeds)
        logits = self.sm_fc(lstm_out.view(-1, self.embedding_dim))
        return logits, hidden


def repackage_hidden(h):
    """Wraps hidden states in new Variables, to detach them from their history."""
    if type(h) == Variable:
        return Variable(h.data)
    else:
        return tuple(repackage_hidden(v) for v in h)
