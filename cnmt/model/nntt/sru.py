import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import random


class SRU_layer(nn.Module):
    """
    parallel teacher forcing algo for sru
    """

    def __init__(self, in_dim, out_dim, **kwargs):

        self.in_dim = in_dim
        self.out_dim = out_dim
        self.W_f = nn.Linear(self.in_dim, self.out_dim)
        self.W = nn.Linear(self.in_dim, self.out_dim)
        self.W_r = nn.Linear(self.in_dim, self.out_dim)
        self.v_f = nn.Linear
        pass

    def init_hidden(self, batch_size):
        return torch.zeros((batch_size, self.in_dim))

    def forward_layer(self, x, mask):
        """
        x: (batch, seq_length, in_dim)
        mask: (batch, seq_length)
        c: (batch, in_dim)
        """

        x, mask = x.permute(1, 0, 2), mask.permute(1, 0)
