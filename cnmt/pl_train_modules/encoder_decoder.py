import random

import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from loguru import logger

from cnmt.model.rnn_encoder_decoder import (
    LightningEmbed,
    Encoder,
    Attention,
    Decoder,
    Seq2Seq,
)


class RNNSeq2SeqTrainModule(pl.LightningModule):
    def __init__(
        self,
        seq2seq_model=None,
        encoder_config=None,
        decoder_config=None,
        seq2seq_config=None,
        ignore_token=0,
        lr=3e-4,
        tokenizer=None,
        voc_embed_size=None,
        src_pad_idx=None,
        include_lang_id=False,
        train_both_ways=False,
        auto_encoding=False,
        back_translation=False,
        unsupervised_dataset_ae=False,
        back_trans_start=0,
        max_length=100,
        supervised_unsupervised_ratio=1,
        **kwargs,
    ):
        super().__init__()
        self.max_length = max_length
        self.supervised_unsupervised_ratio = supervised_unsupervised_ratio
        self.unsupervised_dataset_ae = unsupervised_dataset_ae
        self.back_translation = back_translation
        self.back_trans_start = back_trans_start
        self.auto_encoding = auto_encoding
        self.include_lang_id = include_lang_id
        self.train_both_ways = train_both_ways
        self.tokenizer = tokenizer
        self.voc_embed_size = voc_embed_size
        self.src_pad_idx = src_pad_idx
        if not seq2seq_config:
            self.seq2seq = seq2seq_model
            self.tokenizer = tokenizer
            self.input_dim = len(tokenizer.vocab)
            self.attention = self.seq2seq.decoder.attention
        else:
            self.attention = Attention(**decoder_config)

            if self.tokenizer and not voc_embed_size:
                self.input_dim = len(tokenizer.vocab)
                encoder = Encoder(input_dim=self.input_dim, **encoder_config)
                decoder = Decoder(
                    output_dim=self.input_dim,
                    attention=self.attention,
                    **decoder_config,
                )

            elif not self.voc_embed_size:
                self.input_dim = self.voc_embed_size
                encoder = Encoder(input_dim=self.input_dim, **encoder_config)
                decoder = Decoder(
                    output_dim=self.input_dim,
                    attention=self.attention,
                    **decoder_config,
                )

            else:
                encoder = Encoder(**encoder_config)
                decoder = Decoder(attention=self.attention, **decoder_config)
                self.input_dim = len(tokenizer.vocab)

            print(f"embed_dim: {self.input_dim}")
            if self.tokenizer and not self.src_pad_idx:
                self.seq2seq = Seq2Seq(
                    encoder=encoder, decoder=decoder, **seq2seq_config
                )

            elif self.src_pad_idx:
                self.seq2seq = Seq2Seq(
                    encoder=encoder,
                    decoder=decoder,
                    src_pad_idx=self.src_pad_idx,
                    **seq2seq_config,
                )

            else:
                self.seq2seq = Seq2Seq(
                    encoder=encoder, decoder=decoder, **seq2seq_config
                )

        # the ignore index should be the padding id
        self.criterion = nn.CrossEntropyLoss(ignore_index=ignore_token)
        self.lr = float(lr)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def get_loss_from_triplet(self, src, src_len, trg, lang_id=0, ae_step=False):
        batch = (
            src,
            src_len,
            trg,
        )

        output = self(batch, lang_id=lang_id, ae_step=ae_step)
        batch_size, seg_length, voc_size = output.shape

        output = output.reshape(batch_size * seg_length, voc_size)
        trg = trg.reshape(batch_size * seg_length)

        trg = trg.to(output.device)
        loss = self.criterion(output, trg)
        return loss

    def forward(self, batch, lang_id=0, ae_step=False):

        src, src_len, trg = batch[:3]
        src = src.transpose(0, 1)
        trg = trg.transpose(0, 1)
        current_lang_id = lang_id if lang_id else 0
        trg_lang_id = int(1 - lang_id) if not ae_step else lang_id
        outputs = self.seq2seq(
            src, src_len, trg, src_lang_id=current_lang_id, trg_lang_id=trg_lang_id
        )
        outputs = outputs.transpose(0, 1)
        return outputs

    @torch.no_grad()
    def translate(self, src, src_len):
        src_transform = src.transpose(0, 1)
        with torch.no_grad():
            result = self.seq2seq.greedy_decode(
                src_transform,
                src_len,
                self.tokenizer.bos_token_id,
                self.tokenizer.eos_token_id,
            )
        return self.tokenizer.decode(result, remove_pad=True)

    def back_translation_step(self, src, src_len, lang_id=0):
        def get_max_length(src_batch):
            # batch index

            length = [len(tokens) for tokens in src_batch]
            max_length = max(length)

            return max_length

        def length_sort(src_batch, trg_batch, src_len):

            descending_index = torch.argsort(src_len, dim=0, descending=True)

            src_len = src_len[descending_index]
            src_batch = src_batch[descending_index]
            trg_batch = trg_batch[descending_index]

            return src_batch, trg_batch, src_len

        translated_from_src = self.translate(src, src_len)
        max_length = min(get_max_length(translated_from_src), self.max_length)
        self.logger.experiment.add_text(
            f"train/src_backtranslate_text_{lang_id}",
            translated_from_src[0],
            global_step=self.global_step,
        )
        translated_from_src = self.tokenizer(
            translated_from_src, max_length=max_length, truncation=True
        )
        translated_from_src_len = (
            translated_from_src != self.tokenizer.pad_token_id
        ).sum(-1)

        translated_from_src, trg, translated_from_src_len = length_sort(
            translated_from_src, src, translated_from_src_len
        )

        translated_from_src_batch = translated_from_src.to(self.device)
        translated_from_src_len_batch = translated_from_src_len.to(self.device)

        loss = self.get_loss_from_triplet(
            translated_from_src_batch,
            translated_from_src_len_batch,
            src,
            lang_id=int(1 - lang_id),
        )

        return loss

    def unsupervised_autoencoding(self, batch):
        (
            src_unsupervised,
            src_unsupervised_len,
            trg_unsupervised,
            trg_unsupervised_len,
        ) = batch[5:9]
        try:
            unsup_0_loss = self.get_loss_from_triplet(
                src_unsupervised,
                src_unsupervised_len,
                src_unsupervised,
                lang_id=0,
                ae_step=True,
            )

            unsup_1_loss = self.get_loss_from_triplet(
                trg_unsupervised,
                trg_unsupervised_len,
                trg_unsupervised,
                lang_id=1,
                ae_step=True,
            )
            unsup_ae_loss = unsup_0_loss + unsup_1_loss
        except Exception as e:
            print(e)
            print(src_unsupervised)
            print(trg_unsupervised)
            unsup_ae_loss = 0

        return unsup_ae_loss

    def supervised_train_step(self, batch, batch_idx):
        # src to trg translation
        src, src_len, trg = batch[:3]
        if not self.train_both_ways:
            loss = self.get_loss_from_triplet(src, src_len, trg)
        else:
            loss = self.get_loss_from_triplet(src, src_len, trg, lang_id=0)
        self.log(
            "train/src_to_trg_loss",
            loss,
            on_step=True,
            on_epoch=False,
            prog_bar=True,
            logger=True,
        )
        # trg to src translation
        if self.train_both_ways:
            trg_len, trg_index = batch[3:5]
            reindex_src = src[trg_index]
            reindex_trg = trg[trg_index]
            reindex_trg_len = trg_len[trg_index]
            backward_loss = self.get_loss_from_triplet(
                reindex_trg, reindex_trg_len, reindex_src, lang_id=1
            )
            loss = loss + backward_loss
            self.log(
                "train/backward_loss",
                backward_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )

        # src to src and trg to trg
        if self.auto_encoding:
            lang_0_loss = self.get_loss_from_triplet(
                src, src_len, src, lang_id=0, ae_step=True
            )
            trg_len, trg_index = batch[3:5]
            reindex_trg = trg[trg_index]
            reindex_trg_len = trg_len[trg_index]
            lang_1_loss = self.get_loss_from_triplet(
                reindex_trg, reindex_trg_len, reindex_trg, lang_id=1, ae_step=True
            )
            ae_loss = lang_0_loss + lang_1_loss
            loss = loss + ae_loss
            self.log(
                "train/ae_loss",
                ae_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )
        return loss

    def unsupervised_train_step(self, batch, batch_idx):
        loss = 0

        if self.back_translation or self.unsupervised_dataset_ae:
            unsup_ae_loss = self.unsupervised_autoencoding(batch)
            self.log(
                "train/unsupervise_ae_loss",
                unsup_ae_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )
            loss = loss + unsup_ae_loss

        # back_translation
        if self.back_translation and self.global_step >= self.back_trans_start:
            (
                src_unsupervised,
                src_unsupervised_len,
                trg_unsupervised,
                trg_unsupervised_len,
            ) = batch[5:9]

            src_back_loss = self.back_translation_step(
                src_unsupervised, src_unsupervised_len, lang_id=0
            )
            trg_back_loss = self.back_translation_step(
                trg_unsupervised, trg_unsupervised_len, lang_id=1
            )

            back_translation_loss = src_back_loss + trg_back_loss
            loss = loss + back_translation_loss

            self.log(
                "train/back_translation_loss",
                back_translation_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )

        return loss

    def training_step(self, batch, batch_idx):
        # save memory for doing supervised and unsupervised in different step
        if (
            (self.global_step % (self.supervised_unsupervised_ratio + 1) != 0)
            or (not self.back_translation)
            or (self.back_translation and self.global_step <= self.back_trans_start)
        ):
            supervised_loss = self.supervised_train_step(batch, batch_idx)
            self.log(
                "train/loss",
                supervised_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )
            return supervised_loss if supervised_loss else None
        else:
            unsupervised_loss = self.unsupervised_train_step(batch, batch_idx)
            self.log(
                "train/loss",
                unsupervised_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )

            return unsupervised_loss if unsupervised_loss else None

    def validation_step(self, batch, batch_idx):
        src, src_len, trg = batch[:3]
        init_trg = trg
        output = self(batch)
        assert output.shape[:-1] == trg.shape
        batch_size, seg_length, voc_size = output.shape

        output = output.reshape(batch_size * seg_length, voc_size)
        label_trg = trg.reshape(batch_size * seg_length)
        label_trg = label_trg.to(output.device)
        loss = self.criterion(output, label_trg)
        # self.log("val_loss", loss)
        return loss, src.cpu(), src_len.cpu(), init_trg.cpu()

    def validation_epoch_end(self, validation_step_outputs):
        if validation_step_outputs:
            losses, src, src_len, trg = zip(*validation_step_outputs)
        else:
            return

        losses = list(losses)

        self.log(
            "val_total_loss",
            sum(losses) / len(losses),
        )
        self.seq2seq.to("cpu")
        generate_text, target_decode_text, source_decode_text = self.sample_decode_ans(
            src, src_len, trg, self.tokenizer, sample_num=10
        )
        generate_text = "||\n".join(generate_text)
        target_decode_text = "||\n".join(target_decode_text)
        source_decode_text = "||\n".join(source_decode_text)
        self.logger.experiment.add_text(
            "valid/generate_text", generate_text, global_step=self.global_step
        )
        self.logger.experiment.add_text(
            "valid/target_decode_text", target_decode_text, global_step=self.global_step
        )
        self.logger.experiment.add_text(
            "valid/source_decode_text", source_decode_text, global_step=self.global_step
        )

        self.seq2seq.to(self.device)

    @torch.no_grad()
    def sample_decode_ans(self, src, src_len, trg, tokenizer, sample_num=10):

        sample_batch = random.choice(list(range(len(src))))
        src = src[sample_batch]
        src_len = src_len[sample_batch]
        trg = trg[sample_batch]
        assert src.shape[0] == trg.shape[0]
        sample_num = min(trg.shape[0], sample_num)

        sample_index = torch.tensor(random.sample(list(range(sample_num)), sample_num))
        sample_src = src[sample_index]
        sample_src_transform = sample_src.transpose(0, 1)
        sample_src_len = src_len[sample_index]
        sample_trg = trg[sample_index]

        target_decode_text = tokenizer.decode(sample_trg, remove_pad=True)
        source_decode_text = tokenizer.decode(sample_src, remove_pad=True)
        self.seq2seq.eval()
        generate_text = tokenizer.decode(
            self.seq2seq.greedy_decode(
                sample_src_transform,
                sample_src_len,
                tokenizer.bos_token_id,
                tokenizer.eos_token_id,
            ),
            remove_pad=True,
        )
        self.seq2seq.train()
        return generate_text, target_decode_text, source_decode_text
