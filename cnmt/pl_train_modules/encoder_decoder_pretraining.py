import random

import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from loguru import logger
import traceback
import sys

from cnmt.model.rnn_encoder_decoder import (
    LightningEmbed,
    Encoder,
    Attention,
    Decoder,
    Seq2Seq,
)
from cnmt.pl_train_modules.encoder_decoder import RNNSeq2SeqTrainModule


class RNNSeq2SeqPretrainModule(RNNSeq2SeqTrainModule):
    def __init__(self, augmented_data_name=[], unsupervised_data_name=[], **kwargs):
        super().__init__(**kwargs)
        self.augmented_data_name = augmented_data_name
        self.unsupervised_data_name = unsupervised_data_name

    def augmented_autoencoding(self, batch):
        (src0_unsupervised, src0_unsupervised_len, trg0_unsupervised,) = list(
            batch
        )[:3]
        unsup_0_loss = self.get_loss_from_triplet(
            src0_unsupervised,
            src0_unsupervised_len,
            trg0_unsupervised,
            lang_id=0,
            ae_step=True,
        )

        if len(list(batch)) <= 3:
            return unsup_0_loss
        (
            src1_unsupervised,
            src1_unsupervised_len,
            trg1_unsupervised,
        ) = batch[3:6]
        unsup_1_loss = self.get_loss_from_triplet(
            src1_unsupervised,
            src1_unsupervised_len,
            trg1_unsupervised,
            lang_id=1,
            ae_step=True,
        )

        unsup_ae_loss = unsup_0_loss + unsup_1_loss

        return unsup_ae_loss

    def unsupervised_autoencoding(self, batch):
        (
            src_unsupervised,
            src_unsupervised_len,
            trg_unsupervised,
            trg_unsupervised_len,
        ) = batch[:4]
        unsup_0_loss = self.get_loss_from_triplet(
            src_unsupervised,
            src_unsupervised_len,
            src_unsupervised,
            lang_id=0,
            ae_step=True,
        )

        unsup_1_loss = self.get_loss_from_triplet(
            trg_unsupervised,
            trg_unsupervised_len,
            trg_unsupervised,
            lang_id=1,
            ae_step=True,
        )
        unsup_ae_loss = unsup_0_loss + unsup_1_loss
        return unsup_ae_loss

    def training_step(self, batch, batch_idx):

        torch.cuda.empty_cache()
        try:
            supervised_loss = 0
            unsupervised_loss = 0
            for name in self.augmented_data_name:
                loss = self.augmented_autoencoding(batch[name])
                supervised_loss = supervised_loss + loss
                self.log(
                    f"train/augmented/{name}_loss",
                    loss,
                    on_step=True,
                    on_epoch=False,
                    prog_bar=False,
                    logger=True,
                )
            self.log(
                "train/augmented/loss",
                supervised_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )
            #    return supervised_loss if supervised_loss else None
            for name in self.unsupervised_data_name:
                loss = self.unsupervised_autoencoding(batch[name])
                self.log(
                    f"train/unsupervised/{name}_loss",
                    loss,
                    on_step=True,
                    on_epoch=False,
                    prog_bar=False,
                    logger=True,
                )
                unsupervised_loss = unsupervised_loss + loss
            self.log(
                "train/unsupervised/loss",
                unsupervised_loss,
                on_step=True,
                on_epoch=False,
                prog_bar=True,
                logger=True,
            )
            return unsupervised_loss + supervised_loss
        except Exception as e:
            logger.error(e)
            traceback.print_exc()
            opt = self.optimizers()
            opt.zero_grad()
            self.seq2seq.zero_grad()
            del batch
            torch.cuda.empty_cache()
            unsupervised_loss = None
            supervised_loss = None
            return None

    def validation_step(self, batch, batch_idx):
        # save memory for doing supervised and unsupervised in different step
        supervised_loss = 0
        for name in self.augmented_data_name:
            loss = self.augmented_autoencoding(batch[name])
            supervised_loss = supervised_loss + loss

        unsupervised_loss = 0
        for name in self.unsupervised_data_name:
            loss = self.unsupervised_autoencoding(batch[name])
            unsupervised_loss = unsupervised_loss + loss

        return (unsupervised_loss + supervised_loss).cpu()

    def validation_epoch_end(self, validation_step_outputs):
        losses = list(validation_step_outputs)
        self.log(
            "val/total_loss",
            sum(losses) / len(losses),
        )
