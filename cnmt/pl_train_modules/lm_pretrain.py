import random

import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from loguru import logger

from cnmt.inference.greedy_decode import (
    batch_greedy_decode_no_context,
    batch_random_decode_no_context,
)


class RnnLMTrainModule(pl.LightningModule):
    def __init__(
        self,
        lm=None,
        tokenizer=None,
        lr=3e-4,
        max_length=100,
        data_key=None,
        val_generation_batch_size=5,
        **kwargs,
    ):

        super().__init__()
        self.lm = lm
        self.tokenizer = tokenizer
        self.lr = float(lr)
        self.max_length = max_length
        self.data_key = data_key
        self.criterion = nn.CrossEntropyLoss()
        self.val_generation_batch_size = val_generation_batch_size

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def forward(self, x, x_len):
        trg = x[:, 1:]
        batch_size, seq_len = x.shape

        hidden = self.lm.init_hidden(batch_size)
        if isinstance(hidden, tuple):
            hidden = tuple(h.type_as(x).float() for h in hidden)
        else:
            hidden = hidden.type_as(x).float()

        full_output = []
        total_loss = 0
        count = 0
        for seq_token, t in zip(x.transpose(0, 1), trg):
            valid_index = torch.where(
                (seq_token != self.tokenizer.pad_token_id)
                | (seq_token != self.tokenizer.unk_token_id)
            )
            output, hidden = self.lm(seq_token, hidden)
            valid_output, valid_trg = output[valid_index], t[valid_index]
            loss = self.criterion(valid_output, valid_trg)
            total_loss = total_loss + loss
            full_output.append(output)
            count += 1
        full_output = torch.cat(full_output).transpose(0, 1)
        total_loss = total_loss / count
        return full_output, total_loss

    def training_step(self, batch, batch_idx):
        if self.data_key:
            x, x_len = batch[self.data_key]
        else:
            x, x_len = batch
        _, loss = self(x, x_len)
        self.log(
            "train/loss",
            loss,
            on_step=True,
            on_epoch=False,
            prog_bar=True,
            logger=True,
        )
        return loss

    def validation_step(self, batch, batch_idx):
        if self.data_key:
            x, x_len = batch[self.data_key]
        else:
            x, x_len = batch
        _, loss = self(x, x_len)
        return loss

    def validation_epoch_end(self, validation_step_outputs):

        if validation_step_outputs:
            losses = list(validation_step_outputs)
        random_generation = batch_random_decode_no_context(
            self.lm, self.tokenizer, batch_size=self.val_generation_batch_size, gpu=True
        )
        # greedy_generation = batch_greedy_decode_no_context(
        #    self.lm, self.tokenizer, batch_size=self.val_generation_batch_size, gpu=True
        # )
        for i in range(self.val_generation_batch_size):
            print("random: ", random_generation[i])
            # print("greedy: ", greedy_generation[i])
            self.logger.experiment.add_text(
                f"val/random_generate_text_{i}",
                random_generation[i],
                global_step=self.global_step,
            )
            # self.logger.experiment.add_text(
            #    f"val/greedy_generate_text_{i}", greedy_generation[i], global_step=self.global_step
            # )
        self.log(
            "val/total_loss",
            sum(losses) / len(losses),
        )
