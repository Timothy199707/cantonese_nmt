#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import importlib
import random

from loguru import logger


def get_tokenizer_construct_fn(desc=None, tokenizer_name=None, **kwargs):
    def construct_tokenzier(**kwargs):
        fname, function_name = tokenizer_name.split(".")
        module = importlib.import_module(f"cnmt.tokenizer.{fname}")
        fn = getattr(module, function_name, None)
        tokenizer_instance = fn(**desc)
        return {"tokenizer": tokenizer_instance, **kwargs}

    return [construct_tokenzier]


def get_vocab_size(tokenizer):
    return len(tokenizer.vocab)
