import regex as re


class BaseTokenizer:
    def __init__(
        self,
        vocab_fname,
        process_vocab_fn=None,
        bos_token="<s>",
        eos_token="</s>",
        unk_token="<unk>",
        pad_token="<pad>",
        **kwargs
    ):
        if process_vocab_fn:
            self.vocab = process_vocab_fn(vocab_fname)
        else:
            self.vocab = self.create_vocab_embed(vocab_fname)

        self.bos_token = bos_token
        self.eos_token = eos_token
        self.unk_token = unk_token
        self.pad_token = pad_token

        self.vocab = [bos_token, eos_token, unk_token, pad_token] + self.vocab

        self.bos_token_id = self.vocab.index(bos_token)
        self.eos_token_id = self.vocab.index(eos_token)
        self.unk_token_id = self.vocab.index(unk_token)
        self.pad_token_id = self.vocab.index(pad_token)

        self.special_tokens = [
            self.bos_token,
            self.eos_token,
            self.unk_token,
            self.pad_token,
        ]

        self.special_tokens_ids = [self.vocab.index(t) for t in self.special_tokens]

    def create_vocab_embed(self, fname):
        with open(fname, "r") as f:
            words = [re.sub(r"\n", "", w) for w in f.readlines()]
        return words
