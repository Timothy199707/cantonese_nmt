import regex as re
import torch
from loguru import logger

from cnmt.tokenizer.base_tokenizer import BaseTokenizer


class SpaceSepTokenizer(BaseTokenizer):
    def __init__(self, sep=r"\s", remove_empty=False, **kwargs):
        super().__init__(**kwargs)
        self.sep = sep
        self.segmenter = re.compile(sep)
        self.remove_empty = remove_empty

    def tokenize(self, text):
        result = [self.bos_token] + self.segmenter.split(text) + [self.eos_token]
        if self.remove_empty:
            result = [r for r in result if r]
        return result

    def convert_tokens_to_ids(self, tokens):
        return [
            self.vocab.index(t) if t in self.vocab else self.unk_token_id
            for t in tokens
        ]

    def batch_padding(self, batch_tokens, max_length=None):
        truncation_length = (
            max_length if max_length else max([len(t) for t in batch_tokens])
        )
        batch_after_padding = [
            t + [self.pad_token_id] * (truncation_length - len(t))
            if len(t) <= truncation_length
            else t[:truncation_length]
            for t in batch_tokens
        ]
        mask = [
            [1] * len(t) + [0] * (truncation_length - len(t))
            if len(t) <= truncation_length
            else [1] * truncation_length
            for t in batch_tokens
        ]
        assert all([len(b) == truncation_length for b in batch_after_padding])
        assert all([len(m) == truncation_length for m in mask])
        return batch_after_padding, mask

    def _decode_sentence(self, tokens, remove_pad=False):
        if not remove_pad:
            return [self.vocab[int(t)] for t in tokens]
        else:
            return [
                self.vocab[int(t)]
                for t in tokens
                if (int(t) not in self.special_tokens_ids)
            ]

    def decode(self, batch_tokens, remove_pad=False):
        if isinstance(batch_tokens, torch.Tensor):
            batch_tokens = batch_tokens.cpu().numpy().tolist()
        try:
            if len(torch.tensor(batch_tokens).shape) == 1:
                batch_tokens = [batch_tokens]
        except Exception as e:
            # print(f"error here in tokenizer decoding: {e}")
            if isinstance(batch_tokens[0], int):
                batch_tokens = [batch_tokens]
        return [
            self.sep.join(self._decode_sentence(tokens, remove_pad=remove_pad))
            for tokens in batch_tokens
        ]

    def __call__(
        self,
        sents,
        truncation=True,
        return_tensors=True,
        max_length=None,
        return_mask=False,
    ):
        if isinstance(sents, list):
            tokens = [self.convert_tokens_to_ids(self.tokenize(sent)) for sent in sents]
            tokens, mask = (
                self.batch_padding(tokens, max_length)
                if truncation
                else (tokens, torch.ones_like(torch.tensor(tokens).numpy().tolist()))
            )
        else:
            tokens = self.convert_tokens_to_ids(self.tokenize(sents))
            mask = [1] * len(tokens)

        if return_tensors:
            tokens = torch.tensor(tokens)
            mask = torch.tensor(mask)
        if return_mask:
            print(tokens.shape, mask.shape)
            return tokens, mask
        else:
            return tokens


if __name__ == "__main__":
    tokenizer = SpaceSepTokenizer(
        vocab_fname="/home/vincent/data/yue2zh_all/text.vocab"
    )
    text = "若 果 趕 唔 切 上 機 就 弊 喇 ！"
    print(tokenizer(text))
