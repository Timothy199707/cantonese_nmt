#!/usr/bin/env python
# -*- coding: utf-8 -*-

import importlib
import timeit
from contextlib import nullcontext
from pathlib import Path
from munch import Munch
import yaml

from loguru import logger

import pytorch_lightning as pl


def get_trainer_construct_fn(desc, **kwargs):
    def construct_trainer(**kwargs):
        trainer = pl.Trainer(**desc)
        return {"trainer": trainer, **kwargs}

    return [construct_trainer]


def get_run_train_fn(desc, **kwargs):
    def run_construct_trainer(trainer=None, model=None, dataloader=None, **kwargs):
        trainer.fit(model, dataloader)
        return None

    return [run_construct_trainer]


def load_config(fname):
    with open(fname, "r") as f:
        config = Munch(yaml.load(f))
    return config


def create_blocks(blocks):
    new_blocks = {}
    for k, v in blocks.items():
        block_construct_fn = importlib.import_module(v["provider"])
        block_construct_fn = getattr(block_construct_fn, v["fn"])
        new_blocks[k] = block_construct_fn(**v["parameters"])
    return new_blocks


def run_train(blocks=None, description=None, **kwargs):
    init_kwargs = kwargs
    func_list = []
    for trainer_name, training_parts in description.items():
        for train_fn_name in training_parts:
            func_list += blocks[train_fn_name]
        for func in func_list:
            init_kwargs = func(**init_kwargs)
    return init_kwargs


def run_trainer(config):
    individual_parts = config["blocks"]
    trainer_description = config["trainer"]

    blocks = create_blocks(individual_parts)
    logger.debug(blocks)

    init_kwargs = {}
    init_kwargs = run_train(blocks, trainer_description, **init_kwargs)


class CodeTimer:
    def __init__(self, name, average):
        self.name = " '" + name + "'" if name else ""
        self.average = average

    def __enter__(self):
        self.start = timeit.default_timer()

    def __exit__(self, exc_type, exc_value, traceback):
        self.took = (timeit.default_timer() - self.start) * 1000.0
        self.average[0] += self.took
        self.average[1] += 1
        logger.debug(
            self.name
            + " took: "
            + str(self.took)
            + " ms, "
            + "average(n="
            + str(self.average[1])
            + "): "
            + str(self.average[0] / self.average[1])
            + " ms"
        )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str)
    config_fname = getattr(parser.parse_args(), "config")
    config = load_config(config_fname)
    run_trainer(config)
