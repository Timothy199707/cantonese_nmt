def sort2list(list1, list2, descending=True):
    list1, list2 = (list(t) for t in zip(*sorted(zip(list1, list2))))
    if descending:
        list1.reverse()
        list2.reverse()
    return list1, list2


if __name__ == "__main__":
    listA = [0, 1, 2, 3]
    listB = ["a", "aa", "aaa", "aaaa"]
    print(sort2list(listA, listB))
