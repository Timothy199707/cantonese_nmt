Logbook Readme

##Structure Of Logs
- directory: per project (experinemt from other repo)
 - individual readme
   - link to repo
   - description
   - training sop (or link to sop)
   - what is known so far (this will be update with experiments)
   - to-do in experimenting?
 - directory of all logs
  - experiment summary:
   - dir format "{yyyymmdd}{repo name}{experiment name}"
		- meta data: "meta_data.yaml"
			- name: {name of experiment}
			- description: {target of experiment}
			- data_log: {link to training log if it's stored in cloud (wandb)}
			- server_loc: {location of the server hosting the training}
			- training_commit: {commit id}
			- training_result: {discovery of the training}
		- training config backup: "training_config.yaml"
		- git diff record: "git_diff.txt"
