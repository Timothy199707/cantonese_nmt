from setuptools import setup

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="cnmt",
    packages=["cnmt"],
    install_requires=required,
    extras_require={},
)
